package android.srinivasani.happythings;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

public class Holder extends RecyclerView.ViewHolder {
    public TextView titleText;

    public Holder(View itemView) {
        super(itemView);
        titleText = (TextView) itemView;
    }
}
