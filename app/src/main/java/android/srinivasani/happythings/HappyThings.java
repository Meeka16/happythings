package android.srinivasani.happythings;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import java.util.ArrayList;

public abstract class HappyThings extends SingleFragmentActivity {

    public ArrayList<HappyThings> list = new ArrayList<HappyThings>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected Fragment createFragment() {
        return null;
    }


    @Override
    protected int getLayoutResId() {
        return R.layout.activity_fragment;
        }
    }




