package android.srinivasani.happythings;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

public class Adapter extends RecyclerView.Adapter<Holder> {

    private ArrayList<MyList>MyList ;
    private ActivityCallback activityCallback;

    public Adapter(ActivityCallback activityCallback) {
        this.activityCallback = activityCallback;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(android.R.layout.simple_list_item_1, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(Holder holder, final int position) {
        holder.titleText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                //activityCallback.onPostSelected(redditUri);
            }
        });

        holder.titleText.setText(MyList.get(position).Title);

    }

    @Override
    public int getItemCount() {
        return MyList.size();
    }
}

